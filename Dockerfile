FROM node:6-alpine AS build

WORKDIR /usr/src/app

COPY package*.json ./

COPY /dist ./dist

RUN npm install

#RUN build

FROM node:6-alpine AS deps

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

FROM node:6-alpine

WORKDIR /usr/src/app

COPY --from=deps /usr/src/app/node_modules ./node_modules/
COPY --from=build /usr/src/app/dist ./dist/

EXPOSE 3000

CMD [ "node", "dist/test.js" ]